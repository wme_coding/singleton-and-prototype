import service.StudentService;

import javax.security.auth.login.LoginException;

public class Server {
    private StudentService studentService;

    public Server() {
        this.studentService = new StudentService();
    }

    public void getStudent(String studentLogin) throws LoginException {
        studentService.getStudent(studentLogin);
    }

    //here we try to make student attendance in all classes via cloning his/her uni profile
    //and appending to each subject base
    public void makeAttendanceInAllClasses(String studentLogin) throws LoginException {
        studentService.makeAttendanceInAllClasses(studentLogin);
    }
}
