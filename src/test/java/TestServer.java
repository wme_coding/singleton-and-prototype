import database.DataBase;
import model.Student;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.security.auth.login.LoginException;
import java.util.List;

public class TestServer {
    @Before
    public void setUp(){
        DataBase.getDataBase().addStudent(new Student("Alex", "Rogers", "a.rogers", 25));
        DataBase.getDataBase().addStudent(new Student("Fiona", "Gallagher", "f.gal", 24));
        DataBase.getDataBase().addStudent(new Student("Clint", "Eastwood", "c.east", 25));
    }

    @Test
    public void testAttendance() throws LoginException {
        Server server = new Server();
        server.makeAttendanceInAllClasses("c.east");

        List<Student> math = DataBase.getDataBase().getStudentsAttendanceMath();
        List<Student> bio = DataBase.getDataBase().getStudentsAttendanceGeography();
        List<Student> geo = DataBase.getDataBase().getStudentsAttendanceGeography();

        Assert.assertTrue(math.get(0).getFirstName().equals("Clint"));
        Assert.assertTrue(bio.get(0).getFirstName().equals("Clint"));
        Assert.assertTrue(geo.get(0).getFirstName().equals("Clint"));
    }
}
