package database;

import model.Student;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.List;

public class DataBase {
    private static DataBase dataBase;

    private List<Student> allStudents;

    private List<Student> studentsAttendanceMath;
    private List<Student> studentsAttendanceBiology;
    private List<Student> studentsAttendanceGeography;

    private DataBase() {
        allStudents = new ArrayList<Student>();
        studentsAttendanceMath = new ArrayList<Student>();
        studentsAttendanceBiology = new ArrayList<Student>();
        studentsAttendanceGeography = new ArrayList<Student>();
    }

    //Singleton
    public static DataBase getDataBase(){
        if (dataBase == null) {
            dataBase= new DataBase();
        }
        return dataBase;
    }

    public Student getStudent(String studentLogin) throws LoginException {
        for(Student s: allStudents){
            if (s.getLogin().equals(studentLogin)) {
                return s;
            }
        }
        throw new LoginException();
    }

    public void makeAttendanceOnMath(Student student){
        studentsAttendanceMath.add(student);
    }

    public void makeAttendanceOnBiology(Student student){
        studentsAttendanceBiology.add(student);
    }

    public void makeAttendanceOnGeography(Student student){
        studentsAttendanceGeography.add(student);
    }

    public void addStudent(Student student){
        allStudents.add(student);
    }

    public List<Student> getStudentsAttendanceMath() {
        return studentsAttendanceMath;
    }

    public List<Student> getStudentsAttendanceBiology() {
        return studentsAttendanceBiology;
    }

    public List<Student> getStudentsAttendanceGeography() {
        return studentsAttendanceGeography;
    }
}
