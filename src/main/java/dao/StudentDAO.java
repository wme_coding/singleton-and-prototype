package dao;

import model.Student;

import javax.security.auth.login.LoginException;

public interface StudentDAO {
    Student getStudent(String studentLogin) throws LoginException;

    void makeAttendanceOnMath(Student student);

    void makeAttendanceOnBiology(Student student);

    void makeAttendanceOnGeography(Student student);

}
