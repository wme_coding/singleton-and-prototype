package service;

import dao.StudentDAO;
import daoimpl.StudentDaoImpl;
import model.Student;

import javax.security.auth.login.LoginException;

public class StudentService {
    StudentDAO studentDAO;

    public StudentService() {
        this.studentDAO = new StudentDaoImpl();
    }

    public Student getStudent(String studentLogin) throws LoginException {
        return studentDAO.getStudent(studentLogin);
    }

    //Here we clone student
    public void makeAttendanceInAllClasses(String studentLogin) throws LoginException {
        Student student = getStudent(studentLogin);
        studentDAO.makeAttendanceOnMath(student.clone());
        studentDAO.makeAttendanceOnBiology(student.clone());
        studentDAO.makeAttendanceOnGeography(student.clone());
    }
}
