package daoimpl;

import dao.StudentDAO;
import database.DataBase;
import model.Student;

import javax.security.auth.login.LoginException;

public class StudentDaoImpl implements StudentDAO {
    private DataBase dataBase;

    public StudentDaoImpl(){
        this.dataBase = DataBase.getDataBase();
    }

    @Override
    public Student getStudent(String studentLogin) throws LoginException {
        return dataBase.getStudent(studentLogin);
    }

    @Override
    public void makeAttendanceOnMath(Student student) {
        dataBase.makeAttendanceOnMath(student);
    }

    @Override
    public void makeAttendanceOnBiology(Student student) {
        dataBase.makeAttendanceOnBiology(student);
    }

    @Override
    public void makeAttendanceOnGeography(Student student) {
        dataBase.makeAttendanceOnGeography(student);
    }
}
