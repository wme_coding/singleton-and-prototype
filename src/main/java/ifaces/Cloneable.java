package ifaces;

public interface Cloneable {

    Cloneable clone();
}
