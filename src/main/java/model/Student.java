package model;

import ifaces.Cloneable;

public class Student implements Cloneable {
    private String firstName;
    private String secondName;
    private String login;
    private int age;

    public Student(String firstName, String secondName, String login, int age) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.login = login;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Student clone() {
        return new Student(this.firstName, this.secondName, this.login, this.age);
    }
}
